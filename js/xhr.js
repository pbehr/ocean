var theXHR = new XMLHttpRequest();
var method = 'GET';
var url = 'data.json';

function stateChange() {
  switch (theXHR.readyState) {
    case 0:
      console.log('Ready state 0 = xhr created');
      break;
    case 1:
      console.log('Ready state 1 = open() has been called');
      break;
    case 2:
      console.log('Ready state 2 = headers received');
      break;
    case 3:
      console.log('Ready state 3 = downloading');
      break;
    case 4:
      console.log('Ready state 4 = done');
      if (theXHR.status = 200) {
        var response = JSON.parse(theXHR.responseText);
        console.log(response.person.firstName);
      }
    other:
      console.log('Ready state = ' + theXHR.readyState);
  }
}

theXHR.onreadystatechange = stateChange;
theXHR.overrideMimeType("application/json");
theXHR.open(method, url);
theXHR.send();
